public class Job {

	private int submitTime;
	private int id;
	private int estRuntime;
	private int cpuCores;
	private int memory;
    private int disk;
    

    public void setSubmitTime(int submitTime) {
        this.submitTime = submitTime;
    }

    public int getSubmitTime() {
        return submitTime;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public void setRunTime(int estRuntime) {
        this.estRuntime = estRuntime;
    }

    public int getRunTime() {
        return estRuntime;
    }

    public void setCPUCores(int cpuCores) {
        this.cpuCores = cpuCores;
    }

    public int getCPUCores() {
        return cpuCores;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }
    
    public int getMemory() {
        return memory;
    }

    public void setDisk(int disk) {
        this.disk = disk;
    }

    public int getDisk() {
        return disk;
    }


	Job(int submitTime, int id, int estRuntime, int cpuCores, int memory, int disk) {
       setSubmitTime(submitTime);
       setID(id);
       setRunTime(estRuntime);
       setCPUCores(cpuCores);
       setMemory(memory);
       setDisk(disk);
	}
} 