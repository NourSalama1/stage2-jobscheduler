

public class Server implements Comparable<Server> {

	private int id;
	private String type;
	private int limit;
	private int bootupTime;
	private double rate;
	private int coreCount;
	private int memory;
	private int disk;
	private int state;
	private int availTime;
	private int initCoreCount;
	private int estJobTime;

	public Server()
    {
    	
    }	


public void setID(int id) {
	this.id = id;
}

public int getID() {
	return id;
}

public void setType(String type) {
	this.type = type;
}

public String getType() {
	return type;
}

public void setLimit(int limit) {
	this.limit = limit;
}

public int getLimit() {
	return limit;
}

public void setbootupTime(int bootupTime) {
	this.bootupTime = bootupTime;
}

public int getbootupTime() {
	return bootupTime;
}

public void setcoreCount(int coreCount) {
	this.coreCount = coreCount;
}

public int getcoreCount (){
	return coreCount;
}

public void setMemory(int memory) {
	this.memory = memory;
}

public int getMemory (){
	return memory;
}

public void setDisk(int disk) {
	this.disk = disk;
}

public int getDisk (){
	return disk;
}


public void setRate(double rate) {
	this.rate = rate;
}

public double getRate (){
	return rate;
}


public void setState(int state) {
	this.state = state;
}

public int getState() {
	return state;
}

public void setAvailableTime(int availTime) {
	this.availTime = availTime;
}

public int getAvailableTime() {
	return availTime;
}

public void setEstJobTime (int est){
	this.estJobTime=est;
}

public int getEstJobTime(){
	return estJobTime;
}

// Type of server that will be read by XML parser
 
public Server(int id, String type, int limit, int bootupTime, double rate, int coreCount,  int memory, int disk) {
	setID(id);
	setType(type);
	setLimit(limit);
	setbootupTime(bootupTime);
	setRate(rate);
	setcoreCount(coreCount);
	setMemory(memory);
	setDisk(disk);
	
	}

public void setInitCoreCount(int coreCount) {
	this.initCoreCount = coreCount;
} 

public int getInitCoreCount(){
	return initCoreCount;
}


public Server(String type, int id, int state, int availTime, int coreCount, int memory, int disk) {
	setType(type);
	setID(id);
	setState(state);
	setAvailableTime(availTime);
	setcoreCount(coreCount);
	setMemory(memory);
	setDisk(disk);


	}

	@Override
	public int compareTo(Server serv) {
		if (this.getcoreCount() > serv.getcoreCount()) {
			return 1;
	}  else if  (this.getcoreCount() == serv.getcoreCount()) {
		return 0;
	} else {
	return -1;
		}
	}

		
}