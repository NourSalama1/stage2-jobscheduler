
/*
 	COMP3100 - Distributed Systems Project
 	Stage 2
   	Author: Nour Salama
*/

import java.net.*;
import java.util.ArrayList;
import java.util.Comparator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.io.*;

public class Client {

	// Inp and Out socket streams
	private Socket socket = null;
	private DataInputStream input = null;
	private DataOutputStream out = null;

	// Responses for the server
	private static final String NONE = "NONE";
	private static final String DOT = ".";
	private static final String OK = "OK";
	private static final String QUIT = "QUIT";
	
	// Commands for the client
	private static final String HELO = "HELO";
	private static final String AUTH = "AUTH";
	private static final String REDY = "REDY";
	private static final String SYSTEM_ADDR = "ds-system.xml";
	private static List<Server> servers_list = new ArrayList<>();
	private List<Server> resourcesList;
	ArrayList<Server>schedulingServers = new ArrayList<>();
	private static final String SCHD = "SCHD %s %s %s";
	private static final String RESCC = "RESC Capable ";

	// Algorithms
	private static final String FIRST_FIT = "ff";
	private static final String BEST_FIT = "bf";
	private static final String WORST_FIT = "wf";
	private static final String NEW_FIT = "best_util_and_cost";
	private static String algorithm = "";

	
	

	// Primary constructor
	public Client(String address, int port) throws IOException {
		try {
			openSession(address, port);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// This function is responsible for sending a message
	public String sendMessage(String msg) {
		String response = "";	// Initially - empty response
		try {
			out.write(msg.getBytes());
			response = receiveMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		while(response.trim().isEmpty()) {
			response = receiveMessage();
		}
		return response;
	}

	public void openSession(String addr, int port) {
		// Generate a new session between both
		try {
			//Initialisation section for input and output streams
			socket = new Socket(addr, port);
			input = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());

			// Once connection is sucessful - update
			System.out.println("Connection Successful");

			// The client will then respond with HELO
			String response = sendMessage(HELO);

			// The Server will then respond with OK and the Client will send AUTH
			response = sendMessage(AUTH);

			// Server will then ack and respond with OK, In addition, the available servers will be assigned into a list
			servers_list = servers(SYSTEM_ADDR);
			List<Server> list = new ArrayList<>();
			list = smallestToLargest(servers_list);

			while (!(response.equals(QUIT))) {
				response = sendMessage(REDY);
				if (response.equals(NONE)) {
					System.out.println("Time to quit");
					response = sendMessage(QUIT);

				} else if (!(response.equals(NONE))) {
					String[] job_n = response.split(" ");
					Job job = new Job(Integer.parseInt(job_n[1]), Integer.parseInt(job_n[2]), Integer.parseInt(job_n[3]),
							Integer.parseInt(job_n[4]), Integer.parseInt(job_n[5]), Integer.parseInt(job_n[6]));

					if (algorithm.equals("")) {	// Set algo to default
						allToLargest(job.getID());
					}
					else {
						response = sendMessage(RESCC + job.getCPUCores() + " " + job.getMemory() + " " + job.getDisk());
						resc(response);
						Server sel_srvr = null;
						if (algorithm.equals(FIRST_FIT)) {
							sel_srvr = findFirstFit(job, list);
							response = sendMessage(((String.format(SCHD, job.getID(), sel_srvr.getType(),
									sel_srvr.getID()))));
						}
						else if (algorithm.equals(BEST_FIT)) {
							sel_srvr = findBestFit(job, resourcesList);
							response = sendMessage(((String.format(SCHD, job.getID(), sel_srvr.getType(),
									sel_srvr.getID()))));
						}
						else if (algorithm.equals(WORST_FIT)) {
							sel_srvr = findWorstFit(job, resourcesList);
							response = sendMessage(((String.format(SCHD, job.getID(), sel_srvr.getType(),
									sel_srvr.getID()))));
						}
						else if (algorithm.equals(NEW_FIT)) {
							sel_srvr = findBestUtilAndCost(job);
							response = sendMessage(((String.format(SCHD, job.getID(), sel_srvr.getType(),
									sel_srvr.getID()))));
						}
						sel_srvr.setEstJobTime(job.getSubmitTime()+job.getRunTime());
						findSchedulingServers(sel_srvr);
					}
				}
			}
			closeSession();
		} catch (IOException io) {
			System.out.println(io);
		}
	}


	// Cycle through every server resource and fill resourcesList
	public List<Server> resc(String response) {
		resourcesList = new ArrayList<Server>();
		if (response.matches("DATA")) {
			while (!(response.equals(DOT))) { // Server will send "." once no further information to send
				response = sendMessage(OK);
				if (!(response.equals(DOT))) {
					String[] srvrRsp = response.split(" ");
					resourcesList.add(new Server(srvrRsp[0], Integer.parseInt(srvrRsp[1]),
							Integer.parseInt(srvrRsp[2]), Integer.parseInt(srvrRsp[3]),
							Integer.parseInt(srvrRsp[4]), Integer.parseInt(srvrRsp[5]),
							Integer.parseInt(srvrRsp[6])));

				}
			}
		}
		return resourcesList;
	}


	// Return server response value after scheduling to largest srvr
	public String allToLargest(int j_id) {
		String resp = sendMessage(((String.format(SCHD, j_id, findLargest().getType(), "0"))));
		System.out.println("The value of the response after sending SCHD is : " + j_id);
		return resp;
	}


	// Find the largest server
	public Server findLargest() {
		Server max = servers_list.get(0);
		for (Server server : servers_list) {
			if (server.getcoreCount() > max.getcoreCount()) {
				max = server;
			}
		}
		return max;
	}

	
	// Put server from s-mallest to largest
	public List<Server> smallestToLargest(List<Server> serverslist) {
		serverslist.sort(Comparator.comparing(Server::getcoreCount));
		return serverslist;
	}

	
	// First fit algorithm
	public Server findFirstFit(Job job, List<Server> list) {
		boolean located = false;
		for (Server server : list) {
			for (Server mainSrvr : resourcesList) {
				if (server.getType().equals(mainSrvr.getType())) {
					if (mainSrvr.getcoreCount() >= job.getCPUCores() && mainSrvr.getDisk() >= job.getDisk()
							&& mainSrvr.getMemory() >= job.getMemory() && mainSrvr.getState() != 4) { 
						located = true; 
						return mainSrvr;
					}
				}
			}
		}
		if (!located) { 							
			for (Server _server : servers_list) { 
				Server main = null;
				if (_server.getcoreCount() >= job.getCPUCores() && _server.getDisk() >= job.getDisk()
						&& _server.getMemory() >= job.getMemory() && _server.getState() != 4) {
					main = _server;  
					main.setID(0);
					return main;
				}
			}
		}
		return null;
	}


	// Best fit algorithm
	public Server findBestFit(Job job, List<Server>list) {
		boolean located = false;
		int minimum = Integer.MAX_VALUE;
		int best_fit = Integer.MAX_VALUE;
		Server mostSuited = null;
		int fValue; 
		for (Server server : list) {
			if ((server.getcoreCount() >= job.getCPUCores() && server.getMemory() >= job.getMemory()
					&& server.getDisk() >= job.getDisk())) {
				fValue = server.getcoreCount() - job.getCPUCores();
				if ((fValue < best_fit) || (fValue == best_fit && server.getAvailableTime() < minimum)) {
					best_fit = fValue;
					minimum = server.getAvailableTime();
					if (server.getState() == 0 || server.getState() == 1 || server.getState() == 2
							|| server.getState() == 3) {
						located = true;
						mostSuited = server;
					}
				}
			}
		}
		if (located) {
			return mostSuited;
		} else {
			Server secondaryServer = null;
			int newfVal;
			int sub = Integer.MAX_VALUE;

			for (Server serv : servers_list) {
				newfVal = serv.getcoreCount() - job.getCPUCores();
				if (serv.getMemory() >= job.getMemory() && serv.getDisk() >= job.getDisk() && newfVal >= 0
						&& newfVal < sub) {
					sub = newfVal;
					secondaryServer = serv;
				}
			}
			secondaryServer.setID(0);
			return secondaryServer;
		}
	}

	
	// Worst fit algorithm
	public Server findWorstFit(Job job, List<Server>list) {
		boolean located = false;
		boolean temp = false;
		int aFit = Integer.MIN_VALUE;
		int worstFit = Integer.MIN_VALUE;
		Server suited = null;
		Server suited2 = null;
		int fitVal;
		for (Server server : list) {
			if ((server.getcoreCount() >= job.getCPUCores() && server.getMemory() >= job.getMemory()
			&& server.getDisk() >= job.getDisk())) {
		fitVal = server.getcoreCount() - job.getCPUCores();
		if (fitVal > worstFit
				&& (server.getState() == 2 || server.getState() == 3) && (server.getAvailableTime() == -1 || server.getAvailableTime() == job.getSubmitTime())) {
			worstFit = fitVal;
			located = true;
			suited = server;

		} else if (fitVal > aFit && server.getState() != 3 && server.getState() != 2) {
			aFit = fitVal;
			temp = true;
			suited2 = server;
				}
			}
		}
		if (located == true) {
			return suited;
		}
		if (temp == true) {
			return suited2;
		} else {
			Server secondaryServer = null;
			int updatedFValue;
			int sub = Integer.MIN_VALUE;
			for (Server serv : servers_list) {
				updatedFValue = serv.getcoreCount() - job.getCPUCores();
				if (serv.getMemory() >= job.getMemory() && serv.getDisk() >= job.getDisk() && updatedFValue > sub) {
					sub = updatedFValue;
					secondaryServer = serv;
				}
			}
			secondaryServer.setID(0);
			return secondaryServer;
		}

	}

	
	// Find the scheduling server
	public void findSchedulingServers(Server srvr){    
		if (schedulingServers.contains(srvr)){
			return;
		}	
		else {
			schedulingServers.add(srvr);	
		}	
	}

	
	// Implemented algorithm
	public Server findBestUtilAndCost(Job j) {
    	Server srvr = null;
    	if(schedulingServers.size()==0)
		srvr = findBestFit(j, resourcesList);
    	else {
    		srvr = findBestFit(j, schedulingServers);
    		if(srvr == null) {
				srvr = findBestFit(j, resourcesList);
			}
    	}
    	return srvr;
    }

	
	// List of servers
	public List<Server> servers(String addr) {
		List<Server> lis = new ArrayList<>();
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = fac.newDocumentBuilder();
			Document doc = builder.parse("system.xml");
			NodeList nodeList = doc.getElementsByTagName("server");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					lis.add(new Server(i, (eElement.getAttribute("type")),
							Integer.parseInt(eElement.getAttribute("limit")),
							Integer.parseInt(eElement.getAttribute("bootupTime")),
							Double.parseDouble(eElement.getAttribute("rate")),
							Integer.parseInt(eElement.getAttribute("coreCount")),
							Integer.parseInt(eElement.getAttribute("memory")),
							Integer.parseInt(eElement.getAttribute("disk"))));
				}
			}
		} catch (Exception e) {
		}
		return lis;
	}


	// Receive responses
	public String receiveMessage() {
		StringBuilder resp = new StringBuilder();
		try {
			int avail = input.available();
			for (int i = 0; i < avail; i++) {
				resp.append((char) input.read());
			}
		} catch (IOException e) {
			closeSession();
		}
		return resp.toString();
	}


	// CLose the running session
	public void closeSession() {
		try {
			input.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void main(String args[]) throws IOException {
		// Argument properties
		if (args.length == 2) {
			if (args[0].equals("-a")) {
				if (args[1].equals(FIRST_FIT)) {
					algorithm = FIRST_FIT;
					System.out.println("The current algorithm is: " + algorithm);

				} else if (args[1].equals(BEST_FIT)) {
					algorithm = BEST_FIT;
					System.out.println("The current algorithm is: " + algorithm);

				} else if (args[1].equals(WORST_FIT)) {
					algorithm = WORST_FIT;
					System.out.println("The current algorithm is: " + algorithm);
				}

				else if (args[1].equals(NEW_FIT)) {
					algorithm = NEW_FIT;
					System.out.println("The current algorithm is: " + algorithm);
				}
			}
		}
		Client client = new Client("127.0.0.1", 50000);
	}

}
